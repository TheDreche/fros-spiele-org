fros-spiele-org
===============

![Screenshot of the final site](images/screenshot-1.png)

This is my imagination of `http://fros-spiele.org`

Everything which follows will give special instructions on how to use this
on a server owned by you.

Special usage instructions
--------------------------
In order to use this, two additional files are needed:
* rows.json
* featured.txt
Both have to be moved into `www/sources`.

### rows.json
This file can be automatically created via the python-script
`mksiteAuto.py`. It reads the data of `https://api.scratch.mit.edu` and
creates a file called `rows.json`. In this file, all data of the presented
projects are saved. It has to be moved to `www/sources`.

The other file which is maybe generated, `cache.json`, is more useful for
debugging the script. It caches the data so the data hasn't to be
refetched. By default this file shouldn't be generated. If it should be
generated, set `USE_CACHE` to `True`:

	6 | USE_CACHE = True

### featured.txt
This file specifies the projects which should appear in the featured bar at
the top of the page.  It contains URLs to scratch projects which have to
appear at least once in `rows.json`. All of them have to be on one line and
of the form `https://scratch.mit.edu/projects/xxx`. There is a special
keyword which can be in a line instead: `__random__`: It will pick a random
project out of rows.json.

White spaces at the beginning or end of the line are ignored and empty
lines are skipped (only-whitespace lines are empty). The file needs not to
end in a trailing newline.

Also move this file to `www/sources`.

Debugging featured.txt
----------------------
In the case that a featured project doesn't appear in the featured bar the
reason can be that the project is not found in `rows.json`. For checking
this, view the console output.

// vim:filetype=markdown:noexpandtab:shiftwidth=8:tabstop=8:textwidth=75:
