function unexpandscratchprojects() {
	document.querySelectorAll('div.projects.expand').forEach(function(u) {u.classList.remove('expand')});
}

function expandscratchprojects(even) {
	t = even.target;
	while(t != null && !(t.matches('div.projects'))) {
		t = t.parentElement;
	}
	if(t.matches('div.projects')) {
		t.classList.toggle('expand');
	}
}

function addListeners() {
	document.querySelectorAll('div.projects > div.top > button.fold').forEach(function(e) {e.addEventListener("click", expandscratchprojects)})
}

addListeners()
