var featured = null;
var content = null;

function init(data, feat) {
	function getFeaturedRow() {
		return {'text': 'Featured', 'projects': feat};
	}
	var generate = data; // Generate rows
	generate.unshift(getFeaturedRow());
	var to = document.getElementById('insertrowshere'); // Where to generate it to
	var paren = to.parentNode; // Parent of that
	var empty = []; // Nodes which will get random projects
	var found = {}; // Found entries (maps urls to data)
	var pickOf = []; // All data needed for choosing a random project
	var initLater = {}; // Maps urls to lists of not initialized projects
	
	
	function initProject(container, data) {
		var a1 = document.createElement("a");
		a1.href = data['url'];
		var i = document.createElement("img");
		i.src = data['img'];
		a1.appendChild(i);
		var d1 = document.createElement("div");
		d1.classList.add('title');
		var t1 = document.createTextNode(data['title']);
		d1.appendChild(t1);
		a1.appendChild(d1);
		container.appendChild(a1);
		var d2 = document.createElement('div');
		d2.classList.add('creator');
		var t2 = document.createTextNode("by ");
		d2.appendChild(t2);
		var a2 = document.createElement("a");
		a2.href = data['creatorlink'];
		var t3 = document.createTextNode(data['creator']);
		a2.appendChild(t3);
		d2.appendChild(a2);
		container.appendChild(d2);
	};
	
	// Generate rows
	generate.forEach(function(item) {
		var row = document.createElement("div");
		row.classList.add("projects");
		var head = document.createElement("div");
		head.classList.add("top");
		var title = document.createElement("h4");
		var place = document.createElement("div");
		place.classList.add('placeholder');
		place.style.height = "31px";
		title.appendChild(place);
		var addto = title;
		if("url" in item) {
			addto = document.createElement("a");
			addto.href = item["url"];
			title.appendChild(addto);
		};
		if("img" in item) {
			img = document.createElement("img");
			img.src = item["img"];
			addto.appendChild(img)
		};
		var text = document.createTextNode(item["text"]);
		addto.appendChild(text);
		head.appendChild(title);
		var expand = document.createElement("button");
		expand.classList.add("fold");
		var expandtext = document.createTextNode("Erweitern");
		expand.appendChild(expandtext);
		head.appendChild(expand);
		row.appendChild(head);
		
		// Create projects
		var projectlist = document.createElement("ul");
		projectlist.classList.add("scroll");
		item['projects'].forEach(function(project) {
			if(project == null) {
				var p = document.createElement("li");
				p.classList.add("project");
				projectlist.appendChild(p);
				empty.push(p);
			} else if(typeof project == "string") {
				if(project.trim() != "") {
					var p = document.createElement("li");
					p.classList.add("project");
					projectlist.appendChild(p);
					if(!initLater.hasOwnProperty(project)) {
						initLater[project] = [p];
					} else {
						initLater[project].push(p);
					};
				}
			} else {
				// Add to found
				if(!found.hasOwnProperty(project['url'])) {
					found[project['url']] = project;
					pickOf.push(project);
				};
				// Add to HTML
				var p = document.createElement("li");
				p.classList.add("project");
				initProject(p, project);
				projectlist.appendChild(p);
			};
		});
		row.appendChild(projectlist);
		paren.insertBefore(row, to);
	});
	// Initialize uninitialized projects
	// Preset ones (featured row)
	for(item in initLater) {
		var sitem = item.trim();
		var url = sitem;
		if(url.endsWith('/')) {
			url = url.substring(0, url.length - 1);
		}
		initLater[item].forEach(function(i) {
			if(sitem == "") {
				// Nothing to do
			} else if(sitem == '__random__') {
				empty.push(i);
			} else if(found[url] == undefined) {
				console.log('Nicht gefundenes Projekt: ' + sitem);
			} else {
				initProject(i, found[url]);
			}
		});
	}
	// Random ones
	empty.forEach(function(node) {
		initProject(node, pickOf[Math.floor(Math.random() * pickOf.length)]);
	});
	to.remove()
	addListeners();
}

function loadFeatured() {
	var req = new XMLHttpRequest();
	req.open('GET', 'sources/featured.txt');
	req.responseType = 'text';
	req.send();
	req.onload = function() {
		featured = req.response.split('\n');
		if(content != null) {
			init(content, featured);
		}
	};
}

function genRows() {
	var req = new XMLHttpRequest();
	req.open('GET', 'sources/rows.json');
	req.responseType = 'json';
	req.send();
	req.onload = function() {
		content = req.response;
		if(featured != null) {
			init(content, featured);
		}
	}
}

loadFeatured();
genRows();
