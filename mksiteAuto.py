#!/usr/bin/python3
import os, requests, time, json

# StudioID of Fros-Spiele
STUDIOID="26617515"
# Whether a cache.json (mainly for debugging) should be generated/used
USE_CACHE=False

# All URLs mustn't end with '/'!
DATA_PROJECT='https://api.scratch.mit.edu/projects/%s'
DATA_USER='https://api.scratch.mit.edu/users/%s'
DATA_USER_PROJECTS='https://api.scratch.mit.edu/users/%s/projects'
DATA_STUDIO_PROJECTS='https://api.scratch.mit.edu/studios/%s/projects?offset=%s'

PATH_USER_IMG=('profile', 'images', '90x90')
PATH_PROJECT_NAME=('title',)
PATH_PROJECT_IMG=('images', '144x108')
PATH_PROJECT_ID=('id',)
PATH_PROJECT_CREATOR=('author', 'username')
PATH_PROJECT_CREATOR_IMG=('author', 'profile', 'images', '90x90')
PATH_STUDIO_PROJECT_CREATOR_IMG=('avatar', '90x90')

URL_PROJECT='https://scratch.mit.edu/projects/%s'
URL_USER='https://scratch.mit.edu/users/%s'
URL_STUDIO_PROJECTS = "https://scratch.mit.edu/studios/%s/projects"

cache: dict = {}
if USE_CACHE and os.path.isfile('cache.json'):
	with open('cache.json', 'r') as f:
		cache = json.loads(f.read())

users = {}

s = None

def geturl(url):
	if url not in cache:
		print('Requesting\t' + url)
		global s
		if s is None:
			s = requests.Session()
		need = True
		while need:
			try:
				cache[url] = s.get(url).json()
			except KeyboardInterrupt:
				exit(130)
			except Exception:
				print('Rerequesting ... (CTRL-C to cancel)')
			else:
				need = False
		print('Waiting\t\t1s')
		time.sleep(1)
	return cache[url]

def get_path(path, data, *args):
	arg = 0
	for i in path:
		if i is None:
			data = data[args[arg]]
			arg += 1
		else:
			data = data[i]
	return data

class Row:
	def __init__(self, name, img = None, url = None):
		self.img = img
		self.title = name
		self.url = url
		self.projects = []
	
	def add(self, project):
		self.projects.append(project)
	
	def json(self):
		r = {'text': self.title, 'projects': [p.json() for p in self.projects]}
		if self.url is not None:
			r['url'] = self.url
		if self.img is not None:
			r['img'] = self.img
		return r

class UserRow(Row):
	def __init__(self, name, img = None):
		if img is None:
			Row.__init__(self, name, get_path(PATH_USER_IMG, geturl(DATA_USER % name)), URL_USER % name)
		else:
			Row.__init__(self, name, img, URL_USER % name)

class Project:
	def __init__(self, number):
		if type(number) == int:
			self.num = number
			self.name = get_path(PATH_PROJECT_NAME, geturl(DATA_PROJECT % number))
			self.creator = get_path(PATH_PROJECT_CREATOR, geturl(DATA_PROJECT % number))
			self.img = get_path(PATH_PROJECT_IMG, geturl(DATA_PROJECT % number))
		elif type(number) == dict or type(number) == int:
			self.num = get_path(PATH_PROJECT_ID, number)
			self.name = get_path(PATH_PROJECT_NAME, number)
			self.creator = get_path(PATH_PROJECT_CREATOR, number)
			self.img = get_path(PATH_PROJECT_IMG, number)
		else:
			raise TypeError
		self.url = URL_PROJECT % self.num
		self.creator_url = URL_USER % self.creator
	
	def json(self):
		return {'url': self.url, 'img': self.img, 'title': self.name, 'creator': self.creator, 'creatorlink': self.creator_url}

def add_studio(num):
	offset = 0
	def add_projects(data):
		assert type(data) == list or type(data) == tuple
		for i, project in enumerate(data):
			print('Progress:\t' + str((i * 100) // len(data)) + '%')
			user = get_path(PATH_PROJECT_CREATOR, geturl(DATA_PROJECT % get_path(PATH_PROJECT_ID, project)))
			if user not in users:
				users[user] = UserRow(user, get_path(PATH_STUDIO_PROJECT_CREATOR_IMG, project))
			users[user].add(Project(get_path(PATH_PROJECT_ID, project)))
	new = geturl(DATA_STUDIO_PROJECTS % (num, offset))
	data = new
	while len(new) == 20:
		offset += 20
		new = geturl(DATA_STUDIO_PROJECTS % (num, offset))
		data += new
	add_projects(data)

def save(data):
	print('Saving ...')
	def fnum(num):
		if num == 0:
			return 'rows.json'
		elif num == 1:
			return 'rows.json~'
		else:
			return 'rows.json~' + str(num)
	names = [fnum(0)]
	while os.path.exists(names[-1]):
		names.append(fnum(len(names)))
	for i in range(len(names)-1, 0, -1):
		os.rename(names[i-1], names[i])
	with open(names[0], 'w') as f:
		f.write(json.dumps(data, sort_keys=True, indent='\t'))

def save_cache():
	with open('cache.json', 'w') as f:
		f.write(json.dumps(cache, separators=(',', ':')))

def main():
	add_studio(STUDIOID)
	rows = [users[r].json() for r in sorted(users.keys())]
	save_cache()
	save(rows)

if __name__ == '__main__':
	main()
